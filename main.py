#!/usr/bin/env python3 
#coding: utf-8

# Стороние модули
from PyQt5.QtWidgets import QApplication, QWidget, QTableWidgetItem
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from sh import tail, grep

import sys, time

# Инициализация интерфейса программы
from ui_main import Ui_Psa

# Модули определения сканирования
from methods import tcpcon

# Время (сек.) за которое программа выводит результат (удаление предыдуших записей)
UPDATETIME = 1

ISDEBUG = True
OWN_IP = '127.0.0.1'

KNOWNFLAGS = ["ACK", "RST", "SYN", "URG", "PSH", "FIN",]

TRACKER = {}
SUSPECTS = {}
IPTABLE_ROWS = 0

STARTLOG  = False
STARTSCAN = False

def Debug(msg):
    if (ISDEBUG):
        print(msg)

class LogHandler(QObject):
    running = False
    newLog = pyqtSignal(str)
    #newIp = pyqtSignal(str, str)

    def startLogging(self):
        for line in tail("-f", "/var/log/kern.log", _iter=True):
            if ("IPT_LOG" in line):
                #Debug(line)
                if (STARTLOG):
                    with open("app.log", "a") as logfile:
                       logfile.write(line)
                    self.newLog.emit(line)
                self.parse_pkt(line)

    def parse_pkt(self, pkt):
        splitted = pkt.split(" ")
        src_ip = ''
        dst_ip = ''
        dst_port = ''
        time_stamp = ''
        packettype = ''

        flags = []

        for line in splitted:
            time_stamp = splitted[5][1:-1]
            if ("SRC=" in line):
                src_ip = line[4:] 
            if ("DST=" in line):
                dst_ip = line[4:] 
            if ("DPT=" in line):
                dst_port = line[4:]
            if ("PROTO=" in line):
                packettype = line[6:]
            if (line in KNOWNFLAGS):
                flags.append(line)
        
        self.track_ip(time_stamp, src_ip, packettype, flags)

    def track_ip(self, time_stamp, src_ip, packettype, flags):
        reason = "..."
        _suspects = {}
        global SUSPECTS
        global TRACKER

        # новый ip
        if (src_ip not in TRACKER):
            ports_visited = tcpcon.TcpCon(src_ip)
            ports_visited.tcpScan(time_stamp, src_ip, packettype, flags)
            TRACKER[src_ip] = ports_visited
        # старый
        else:
            _suspects = TRACKER[src_ip].tcpScan(time_stamp, src_ip, packettype, flags)
            if (_suspects != None):
                SUSPECTS.update(_suspects)

class UpdateHandler(QObject):
    running = False
    newIp = pyqtSignal(str, str)

    def UpdateTable(self):
        global SUSPECTS
        global TRACKER
        time.sleep(2)
        while (True):
            time.sleep(1)
            susp = SUSPECTS.get('suspect')
            reas = SUSPECTS.get('reason')
            if ( ( susp == None ) and ( reas == None) ):
                continue
            Debug("Dict {}: {} {}".format(len(SUSPECTS), susp, reas))
            self.newIp.emit(susp, reas)
            SUSPECTS.clear()
            TRACKER.clear()


class Psa(QWidget, Ui_Psa):
    def __init__ (self):
        super(Psa, self).__init__()
        self.setupui(self)
        # создадим поток
        self.thread = QThread()
        self.threadTimer = QThread()
        # создадим объект для выполнения кода в другом потоке
        self.LogHandler = LogHandler()
        self.UpdateHandler = UpdateHandler()
        # перенесём объект в другой поток
        self.LogHandler.moveToThread(self.thread)
        self.UpdateHandler.moveToThread(self.threadTimer)
        # подключаю сигналы к слотам
        self.LogHandler.newLog.connect(self.addNewLog)
        self.UpdateHandler.newIp.connect(self.addNewIp)

        self.thread.started.connect(self.LogHandler.startLogging)
        self.threadTimer.started.connect(self.UpdateHandler.UpdateTable)

        self.thread.start()
        self.threadTimer.start()
        
        self.btn_launch.clicked.connect(lambda:self.setupLogging())

        self.show()
        
        self.tableip_row = 0

    @pyqtSlot(str)
    def addNewLog(self, string):
        self.logsbox.append(string)

    @pyqtSlot(str, str)
    def addNewIp(self, ip, reason):
        self.tbl_addrs.setItem(self.tableip_row, 0, QTableWidgetItem(ip))
        self.tbl_addrs.setItem(self.tableip_row, 1, QTableWidgetItem(reason))
        self.tableip_row += 1
        self.tbl_addrs.insertRow(self.tableip_row)
        self.tbl_addrs.resizeColumnsToContents()
        self.lb_detectedcount.setText(str(self.tableip_row))

    def setupLogging(self):
        global STARTLOG
        if (STARTLOG):
            self.btn_launch.setText('Включить запись')
            STARTLOG = False
        else:
            self.btn_launch.setText('Отключить запись')
            STARTLOG = True


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Psa()
    sys.exit(app.exec_())