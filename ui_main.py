#!/usr/bin/env python3 
#coding: utf-8

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Psa(object):
    def setupui(self, psawindow):
        psawindow.resize(720,420)
        psawindow.setWindowTitle("Port Scan Alarm")

        tabs = QtWidgets.QTabWidget()

        tab1 = QtWidgets.QWidget()
        tab2 = QtWidgets.QWidget()
        
        #tabs.resize(280,100)
        tabs.addTab(tab1, "Состояние")
        tabs.addTab(tab2, "Журнал")

        self.btn_launch = QtWidgets.QPushButton('Включить запись', self)
        
        self.lb_detectedcount = QtWidgets.QLabel("0");

        self.tbl_addrs = QtWidgets.QTableWidget(self)
        self.tbl_addrs.setColumnCount(2)
        self.tbl_addrs.setRowCount(1)

        # SRC: XX DL: 4, Packets: 3251, Sig count: 73
        self.tbl_addrs.setHorizontalHeaderLabels(["Адрес подозреваемого", "Причина"])

        vbox = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()

        self.tbl_addrs.resizeColumnsToContents()
        
        hbox.addWidget(QtWidgets.QLabel(""),QtCore.Qt.AlignRight)
        hbox.addWidget(self.btn_launch)

        #vbox.addStretch(1)
        #vbox.addLayout(hbox)
        vbox.addWidget(tabs,1)
        #layout.addWidget(QtWidgets.QLabel("Исходный текст"),1,0)

        # 1 вкладка (Общая информация)
        tab1.layout = QtWidgets.QFormLayout()
        pushButton1 = QtWidgets.QPushButton("PyQt5 button")
        tab1.layout.addRow(QtWidgets.QLabel("Зафиксировано: "),self.lb_detectedcount)
        tab1.layout.addRow(self.tbl_addrs)
        tab1.setLayout(tab1.layout)

        # 2 вкладка (Журнал)
        tab2.layout = QtWidgets.QVBoxLayout()
        self.logsbox = QtWidgets.QTextEdit()
        tab2.layout.addLayout(hbox)
        tab2.layout.addWidget(self.logsbox)
        tab2.setLayout(tab2.layout)

        psawindow.setLayout(vbox)