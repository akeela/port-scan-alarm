#SYN vs RST/ACK значение сканирования
ACKRATIO = 10

class TcpCon:
    def __init__(self, ip):
        self.ip = ip
        self.synCount = 0
        self.rstAckCount = 0
        self.xmasCount = 0
        self.finCount = 0
        self.nullCount = 0

    def tcpScan(self, timestamp, ipsrc, packettype, flags):

        sources = {}
        startTime = 0
        endTime = 0

        if(startTime == 0):
            startTime = timestamp
        endTime = timestamp

        # Проверяю, если пакет тсп
        if packettype != "TCP":
            return

        # TODO: SYN scan

        # Считаю SYNs и ACKs каждого сообщения(источника)
        if("SYN" in flags):
            self.synCount += 1
        elif( ("RST" in flags) or ("ACK" in flags) ):
            self.rstAckCount += 1

        #Null
        if (len(flags) == 0):
            self.nullCount += 1

        # Xmas Считаю URG PSH FIN
        if( ("URG" in flags) and ("PSH" in flags) and ("FIN" in flags) ):
            self.xmasCount += 1

        #Fin ПРОВЕРЯЮ ТОЛЬКО FIN ФЛАГ
        #print("len flags {}".format(len(flags)))
        if( ( len(flags) == 1 ) and ("FIN" in flags) ):
            self.finCount += 1
            #print("len flags {}".format(self.finCount))

        # TODO: SYN scan

        #Con
        if( (self.rstAckCount > 0) & (self.synCount > ACKRATIO*self.rstAckCount)):
            return ({'suspect': self.ip, 'reason': "Отправлен TCP пакет с "+str(self.synCount)+" SYN и "+str(self.rstAckCount)+" RST, ACK флагами"})
        #Null
        if (self.nullCount > 0):
            return ({'suspect': self.ip, 'reason': "Отправлен TCP пакет без флагов (NULL)"})

        #Xmas
        if( (self.xmasCount > 0)):
            return ({'suspect': self.ip, 'reason': "Отправлен TCP пакет с FIN, PSH, URG флагами (XMAS)"})
        #Fin
        if( (self.finCount > 0)):
            return ({'suspect': self.ip, 'reason': "Отправлен TCP пакет с FIN флагом (FIN)"})
